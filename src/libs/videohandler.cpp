#include "videohandler.h"

VideoHandler::VideoHandler()
{
    VideoBurner *videoBurner = new VideoBurner;
    videoBurner->moveToThread(&videoBurnerThread);

    connect(this, &VideoHandler::signalVideoData, videoBurner, &VideoBurner::setVideoData);
    connect(this, &VideoHandler::signalVideoBurn, videoBurner, &VideoBurner::initVideoBurn);
    connect(this, &VideoHandler::signalSaveImage, videoBurner, &VideoBurner::saveImage);

    connect(videoBurner, &VideoBurner::needClip1, this, &VideoHandler::setGenerateClip1);
    connect(videoBurner, &VideoBurner::needClip2, this, &VideoHandler::setGenerateClip2);
    connect(videoBurner, &VideoBurner::needClip3, this, &VideoHandler::setGenerateClip3);

    connect(videoBurner, &VideoBurner::burnProcess, this, [&](QString step, int percentage){
        emit burningProcess(step, percentage);
    });
}

VideoHandler::~VideoHandler(){
    videoBurnerThread.quit();
    videoBurnerThread.wait();
}

void VideoHandler::setVideoData(QString fileName) {
    emit signalVideoData(fileName);
}

void VideoHandler::initVideoBurn(bool clip1, int clip1X, int clip1Y,
                                 bool clip2, int clip2X, int clip2Y,
                                 bool clip3, int clip3X, int clip3Y) {

    if (!videoBurnerThread.isRunning())
        videoBurnerThread.start();

    emit signalVideoBurn(clip1, clip1X, clip1Y,
                         clip2, clip2X, clip2Y,
                         clip3, clip3X, clip3Y);

}

void VideoHandler::saveImage(QImage image, int clipNo) {
        emit signalSaveImage(image, clipNo);
}

bool VideoHandler::generateClip1() {
    return m_generateClip1;
}

bool VideoHandler::generateClip2() {
    return m_generateClip2;
}

bool VideoHandler::generateClip3() {
    return m_generateClip3;
}

void VideoHandler::cancel() {

    videoBurnerThread.requestInterruption();
    m_generateClip1 = false;
    m_generateClip2 = false;
    m_generateClip3 = false;

    videoBurnerThread.quit();

    QDir("edited_videos/.temp").removeRecursively();
}

void VideoHandler::setGenerateClip1(bool set) {
    m_generateClip1 = set;
}

void VideoHandler::setGenerateClip2(bool set) {
    m_generateClip2 = set;
}

void VideoHandler::setGenerateClip3(bool set) {
    m_generateClip3 = set;
}

QStringList VideoHandler::getFilesInFolder(QString folder) {
    QDir dir(folder);
    QStringList files = dir.entryList(QStringList() << "*.mp4", QDir::Files);
    return files;
}

QString VideoHandler::getFullPath(QString folder, QString file) {
    return QString("file:%1/%2/%3").arg(QDir::currentPath()).arg(folder).arg(file);
}
