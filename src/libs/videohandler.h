#ifndef VIDEOHANDLER_H
#define VIDEOHANDLER_H

#include <QObject>
#include <QThread>
#include <QDebug>
#include "OpenShot.h"

#include "videoburner.hpp"

class VideoHandler : public QObject
{
    Q_OBJECT
    QThread videoBurnerThread;

public:
    VideoHandler();
    ~VideoHandler();

    Q_PROPERTY(bool generateClip1 READ generateClip1)
    Q_PROPERTY(bool generateClip2 READ generateClip2)
    Q_PROPERTY(bool generateClip3 READ generateClip3)

    Q_INVOKABLE void saveImage(QImage image, int clipNo);
    Q_INVOKABLE void setVideoData(QString videoName);

    Q_INVOKABLE void cancel();
    Q_INVOKABLE void initVideoBurn(bool clip1, int clip1X, int clip1Y,
                                   bool clip2, int clip2X, int clip2Y,
                                   bool clip3, int clip3X, int clip3Y);

    Q_INVOKABLE QStringList getFilesInFolder(QString folder);
    Q_INVOKABLE QString getFullPath(QString folder, QString file);

    bool generateClip1();
    bool generateClip2();
    bool generateClip3();

signals:
    void signalVideoData(QString fileName);
    void signalVideoBurn(bool clip1, int clip1X, int clip1Y,
                         bool clip2, int clip2X, int clip2Y,
                         bool clip3, int clip3X, int clip3Y);

    void signalSaveImage(QImage image, int clipNo);
    void burningProcess(QString step, int percentage);

private:
    bool m_generateClip1 = false;
    bool m_generateClip2 = false;
    bool m_generateClip3 = false;

    void setGenerateClip1(bool set);
    void setGenerateClip2(bool set);
    void setGenerateClip3(bool set);

    void cleanup();
};

#endif // VIDEOHANDLER_H
