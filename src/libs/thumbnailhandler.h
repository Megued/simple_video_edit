#ifndef THUMBNAILHANDLER_H
#define THUMBNAILHANDLER_H

#include <QObject>
#include <QQuickItem>
#include <QQuickPaintedItem>
#include <QPainter>

#include "OpenShot.h"

class ThumbnailHandler : public QQuickPaintedItem
{
    Q_OBJECT

public:
     ThumbnailHandler(QQuickItem *parent = nullptr);
    ~ThumbnailHandler();

    Q_INVOKABLE void getFrame(QString folder, QString fileName);

protected:
     virtual void paint(QPainter *painter) Q_DECL_OVERRIDE;

private:
     QImage current_image;

signals:

};

#endif // THUMBNAILHANDLER_
