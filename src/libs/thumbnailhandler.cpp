#include "thumbnailhandler.h"

ThumbnailHandler::ThumbnailHandler(QQuickItem *parent)
    : QQuickPaintedItem(parent)
{}

ThumbnailHandler::~ThumbnailHandler(){}

void ThumbnailHandler::getFrame(QString folder, QString fileName) {

    QString path = QString("%1/%2").arg(folder).arg(fileName);

    if (QFile::exists(path)) {
        openshot::FFmpegReader r(path.toUtf8().constData());

        // get the first frame for thumbnail and close file
        r.Open();
        std::shared_ptr<openshot::Frame> f = r.GetFrame(1);
        r.Close();

        std::shared_ptr<QImage> previewImage = f->GetImage();

        current_image = QImage(*previewImage);

        update();
    }
    else
    {
        qInfo()<<"file:"<<path<<" does not exist";
    }
}

void ThumbnailHandler::paint(QPainter *painter) {

    if (this->current_image.isNull())
        return;

    QRectF bounding_rect = boundingRect();
    QImage scaled;

    // ensure thumbnail is always scaled right
    if (bounding_rect.width() > bounding_rect.height())
        scaled = this->current_image.scaledToHeight(bounding_rect.height());
    else
        scaled = this->current_image.scaledToHeight(bounding_rect.width());

    QPointF center = bounding_rect.center() - scaled.rect().center();

    painter->drawImage(center, scaled);
}
