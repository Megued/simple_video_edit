#ifndef VIDEOBURNER_HPP
#define VIDEOBURNER_HPP
#include <QObject>
#include <QDebug>
#include <QTimer>
#include <QThread>
#include "OpenShot.h"

class VideoBurner : public QObject
{
    Q_OBJECT

private:
    QString m_videoName;
    openshot::Fraction m_fps;
    openshot::Fraction m_pixelRatio;
    int m_videoHeight;
    int m_videoWidth;
    long m_videoLength;
    int m_videoBitrate;
    float m_videoDuration;

    int m_clip1Images;
    int m_clip1Generated;
    float m_clip1X;
    float m_clip1Y;

    int m_clip2Images;
    int m_clip2Generated;
    float m_clip2X;
    float m_clip2Y;

    int m_clip3Images;
    int m_clip3Generated;
    float m_clip3X;
    float m_clip3Y;

    int m_burnProgress;

    // predefined layers for clips
    QList<openshot::Clip *> m_clip1;
    QList<openshot::Clip *> m_clip2;
    QList<openshot::Clip *> m_clip3;

    QList<openshot::Clip*>::iterator it;
    openshot::Timeline *timeline;

    void setDefaultValues() {
        m_clip1Images = 0;
        m_clip1Generated = 0;
        m_clip1X = 0.0;
        m_clip1Y = 0.0;

        m_clip2Images = 0;
        m_clip2Generated = 0;
        m_clip2X = 0.0;
        m_clip2Y = 0.0;

        m_clip3Images = 0;
        m_clip3Generated = 0;
        m_clip3X = 0.0;
        m_clip3Y = 0.0;

        m_burnProgress = 0;
    }

    void cleanLists() {
        qDeleteAll(m_clip1);
        qDeleteAll(m_clip2);
        qDeleteAll(m_clip3);
        m_clip1.clear();
        m_clip2.clear();
        m_clip3.clear();
    }

    // openshot uses % for clip position... (╯°□°)╯︵ ┻━┻
    float coordinatesToPercentage(float value, QString type) {
        if (type == "width")
            return  value / m_videoWidth;
        if (type == "height")
            return  value / m_videoHeight;
        return 0.0;
    }

    bool generateClip1() {
        emit needClip1(m_clip1Images > m_clip1Generated);
        return  m_clip1Images > m_clip1Generated;
    }

    bool generateClip2() {
        emit needClip2(m_clip2Images > m_clip2Generated);
        return  m_clip2Images > m_clip2Generated;
    }

    bool generateClip3() {
        emit needClip3(m_clip3Images > m_clip3Generated);
        return  m_clip3Images > m_clip3Generated;
    }

    float imagesProcess() {
        return 10 + (50.0 / (float(m_clip1Images + m_clip2Images + m_clip3Images) / float(m_clip1Generated + m_clip2Generated + m_clip3Generated)));
    }

    float videoProcess(int frames) {
        return 70 + (30.0 / float(m_videoLength / float(frames)) );
    }

    void generateObjects() {

        emit burnProcess("Generating clips", m_burnProgress);

        for (int i = 0; i < m_clip1Images; i++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            QString imageName = QString("edited_videos/.temp/clip1/%1.png").arg(i);

            if (QFile(imageName).exists()) {
                m_clip1.push_back(new openshot::Clip(new openshot::QtImageReader(imageName.toUtf8().toStdString())));
            }
        }

        for (int i = 0; i < m_clip2Images; i++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            QString imageName = QString("edited_videos/.temp/clip2/%1.png").arg(i);

            if (QFile(imageName).exists()) {
                m_clip2.push_back(new openshot::Clip(new openshot::QtImageReader(imageName.toUtf8().toStdString())));
            }
        }

        for (int i = 0; i < m_clip3Images; i++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            QString imageName = QString("edited_videos/.temp/clip3/%1.png").arg(i);

            if (QFile(imageName).exists()) {
                m_clip3.push_back(new openshot::Clip(new openshot::QtImageReader(imageName.toUtf8().toStdString())));
            }
        }

        m_burnProgress += 5;

        if (QThread::currentThread()->isInterruptionRequested()) {
            return;
        }

        // make sure we call this only if items exist
        if (m_clip1.length() || m_clip2.length() || m_clip3.length()) {
            emit startSetObjectProperties();
        }
    }

    void setObjectProperties() {

        emit burnProcess("Setting clip properties", m_burnProgress);

        float position1 = 0.0;
        for (it = m_clip1.begin(); it != m_clip1.end(); it++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            (*it)->Layer(1);
            (*it)->Position(position1);
            (*it)->Start(position1);
            (*it)->End(position1 += 0.3);
            (*it)->scale = openshot::SCALE_NONE;
            (*it)->gravity = openshot::GRAVITY_TOP_LEFT;
            (*it)->location_x = m_clip1X;
            (*it)->location_y = m_clip1Y;
            this->timeline->AddClip(&*(*it));
        }

        float position2 = 0.0;
        for (it = m_clip2.begin(); it != m_clip2.end(); it++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            (*it)->Layer(2);
            (*it)->Position(position2);
            (*it)->Start(position2);
            (*it)->End(position2 += 1.0);
            (*it)->scale = openshot::SCALE_NONE;
            (*it)->gravity = openshot::GRAVITY_TOP_LEFT;
            (*it)->location_x = m_clip2X;
            (*it)->location_y = m_clip2Y;
            this->timeline->AddClip(&*(*it));
        }

        float position3 = 0.0;
        for (it = m_clip3.begin(); it != m_clip3.end(); it++) {

            if (QThread::currentThread()->isInterruptionRequested()) {
                return;
            }

            (*it)->Layer(3);
            (*it)->Position(position3);
            (*it)->Start(position3);
            (*it)->End(position3 += 0.5);
            (*it)->scale = openshot::SCALE_NONE;
            (*it)->gravity = openshot::GRAVITY_TOP_LEFT;
            (*it)->location_x = m_clip3X;
            (*it)->location_y = m_clip3Y;
            timeline->AddClip(&*(*it));
        }

        QString path = QString("raw_videos/%1").arg(m_videoName);
        openshot::Clip c2(new openshot::FFmpegReader(path.toUtf8().toStdString()));
        c2.Position(0.0);
        c2.Layer(0);
        timeline->AddClip(&c2);

        m_burnProgress += 5;

        if (QThread::currentThread()->isInterruptionRequested()) {
            return;
        }

        emit startBurnVideo();
    }

    void burnVideo() {

        emit burnProcess("Burning video", m_burnProgress);

        timeline->Open();
        QString tempPath = QString("edited_videos/.temp/%1").arg(m_videoName);
        openshot::FFmpegWriter w(tempPath.toUtf8().toStdString());

        w.SetAudioOptions(true, "aac", 44100, 2, openshot::ChannelLayout::LAYOUT_STEREO, 128000);
        w.SetVideoOptions(true,"libx264" , m_fps, m_videoWidth, m_videoHeight, m_pixelRatio, false, false, m_videoBitrate);

        w.Open();

        for(int end=1; end <= m_videoLength; end ++) {
            if (QThread::currentThread()->isInterruptionRequested()) {
                timeline->Close();
                w.Close();

                cleanLists();
                return;
            }
            w.WriteFrame(timeline, end, end);
            m_burnProgress = videoProcess(end);
            emit burnProcess("Burning video", m_burnProgress);
        }

        timeline->Close();
        w.Close();

        cleanLists();

        if (QThread::currentThread()->isInterruptionRequested()) {
            return;
        }

        emit startCleanup(tempPath);
    }

    void cleanup(QString tempPath) {
        emit burnProcess("Cleanup", 100);
        QFile move(tempPath);
        QString newPath = QString("edited_videos/%1").arg(m_videoName);

        // remove if file exists
        if (QFile(newPath).exists())
            QFile(newPath).remove();

        move.rename(newPath);

        // remove raw file
        QString rawPath = QString("raw_videos/%1").arg(m_videoName);
        if (QFile(rawPath).exists())
            QFile(rawPath).remove();

        QDir("edited_videos/.temp").removeRecursively();
        emit burnProcess("Done", 100);
    }

public:
    VideoBurner(){
        connect(this, &VideoBurner::startGenerateObjects, this, &VideoBurner::generateObjects);
        connect(this, &VideoBurner::startSetObjectProperties, this, &VideoBurner::setObjectProperties);
        connect(this, &VideoBurner::startBurnVideo, this, &VideoBurner::burnVideo);
        connect(this, &VideoBurner::startCleanup, this, &VideoBurner::cleanup);
    };

    virtual ~VideoBurner(){};

signals:

    void startGenerateObjects();
    void startSetObjectProperties();
    void startBurnVideo();
    void startCleanup(QString tempPath);

    void needClip1(bool clip1);
    void needClip2(bool clip2);
    void needClip3(bool clip3);

    void burnProcess(QString workingOn, int pDone);

public slots:
    void setVideoData(QString videoName){

        QString path = QString("raw_videos/%1").arg(videoName);
        openshot::FFmpegReader r(path.toUtf8().toStdString());

        r.Close(); // it's auto closed, but make sure it really is

        m_videoName = videoName;
        // reset all
        m_fps = r.info.fps;
        m_pixelRatio = r.info.pixel_ratio;
        // multiply bitrate so video doesn't loose much quality
        m_videoBitrate = r.info.video_bit_rate * 5;
        m_videoDuration = r.info.duration;
        m_videoWidth = r.info.width;
        m_videoHeight = r.info.height;
        m_videoLength = r.info.video_length;
    }

    void initVideoBurn(bool clip1, int clip1X, int clip1Y,
                       bool clip2, int clip2X, int clip2Y,
                       bool clip3, int clip3X, int clip3Y) {

        setDefaultValues();
        cleanLists();

        emit burnProcess("Starting process", m_burnProgress);

        if (!QDir("edited_videos").exists())
            QDir().mkdir("edited_videos");

        if (!QDir("edited_videos/.temp").exists())
            QDir().mkdir("edited_videos/.temp");

        m_clip1Generated = m_clip2Generated = m_clip3Generated = 0;

        // get max number of each image that fits in video duration
        // create temp folder for images
        if (clip1) {
            m_clip1Images = ceil(m_videoDuration * 10 / 3);

            if (clip1X)
                m_clip1X = this->coordinatesToPercentage(clip1X, "width");

            if (clip1Y)
                m_clip1Y = this->coordinatesToPercentage(clip1Y, "height");

            if (!QDir("edited_videos/.temp/clip1").exists())
                QDir().mkdir("edited_videos/.temp/clip1");

            emit needClip1(m_clip1Images > m_clip1Generated);
        }
        if (clip2) {
            m_clip2Images = ceil(m_videoDuration);

            if (clip2X)
                m_clip2X = this->coordinatesToPercentage(clip2X, "width");

            if (clip2Y)
                m_clip2Y = this->coordinatesToPercentage(clip2Y, "height");

            if (!QDir("edited_videos/.temp/clip2").exists())
                QDir().mkdir("edited_videos/.temp/clip2");

            emit needClip2(m_clip2Images > m_clip2Generated);
        }
        if (clip3) {
            m_clip3Images = ceil(m_videoDuration * 10 / 5);

            if (clip3X)
                m_clip3X = this->coordinatesToPercentage(clip3X, "width");

            if (clip3Y)
                m_clip3Y = this->coordinatesToPercentage(clip3Y, "height");

            if (!QDir("edited_videos/.temp/clip3").exists())
                QDir().mkdir("edited_videos/.temp/clip3");

            emit needClip3(m_clip3Images > m_clip3Generated);
        }

        // create timeline for this video
        this->timeline = new openshot::Timeline(m_videoWidth,m_videoHeight, m_fps, 44100,2, openshot::ChannelLayout::LAYOUT_STEREO);

        m_burnProgress += 10;
    }

    void saveImage(QImage image, int clipNo) {
        QString path;

        // prevent JS from calling this 1 too many times
        if (m_burnProgress > 60)
            return;

        emit burnProcess("Generating overlays", m_burnProgress);

        switch (clipNo) {
        case 1:
            path = QString("edited_videos/.temp/clip1/%1.png").arg(m_clip1Generated);
            m_clip1Generated++;
            break;
        case 2:
            path = QString("edited_videos/.temp/clip2/%1.png").arg(m_clip2Generated);
            m_clip2Generated++;
            break;
        case 3:
            path = QString("edited_videos/.temp/clip3/%1.png").arg(m_clip3Generated);
            m_clip3Generated++;
            break;
        default: {/* do nothing */}
        }

        if (QThread::currentThread()->isInterruptionRequested()) {
            return;
        }

        image.save(path);

        m_burnProgress = imagesProcess();

        // check if all images are generated
        if (!generateClip1() && !generateClip2() && !generateClip3()) {
            emit startGenerateObjects();
        }
    }
};
#endif // VIDEOBURNER_HPP
