import QtQuick 2.12
import ThumbnailHandler 1.0

import "../components"

FocusScope {
    id: main
    anchors.fill: parent

    property string _viewName: "" // raw_videos, edited_videos
    property string _tag: "[main]"

    Component.onCompleted:
    {
        main.forceActiveFocus()

        console.log(_tag, "_viewName:", _viewName)
        thumbnailGrid.model = _videoHandler.getFilesInFolder(_viewName)
    }

    Keys.onEscapePressed: {
        viewHandler.source = "../views/Menu.qml"
    }

    Item {
        anchors.centerIn: parent

        width: parent.width * 0.8
        height: parent.height * 0.8

        visible: thumbnailGrid.model.length > 0

        GridView {
            id: thumbnailGrid
            anchors.fill: parent
            visible: true
            clip: true

            boundsBehavior: Flickable.StopAtBounds

            cellHeight: 158
            cellWidth: 230

            delegate: Item {
                width: thumbnailGrid.cellWidth
                height: thumbnailGrid.cellHeight

                ThumbnailHandler {
                    id: thumbnail
                    height: parent.height - 30
                    width: parent.width - 10

                    anchors.centerIn: parent

                    Component.onCompleted: thumbnail.getFrame(_viewName, modelData)

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true

                        onEntered: {
                            hover.opacity = 0;
                        }
                        onExited: {
                            hover.opacity = 0.6;
                        }

                        onClicked: {
                            viewHandler.setSource("VideoPlayer.qml", {viewName:_viewName, fileName:modelData})
                        }
                    }

                    Rectangle {
                          id: hover
                          anchors.fill: parent
                          color: "#222"
                          opacity: 0.6
                      }
                }
                Rectangle {
                    visible: _viewName === "edited_videos" ? false : true
                    height: 20
                    width: thumbnail.width
                    color: "#111"

                    anchors.top: thumbnail.bottom
                    anchors.horizontalCenter: parent.horizontalCenter

                    Text {
                        text: "Edit"
                        anchors.centerIn: parent
                        font.pointSize: 12
                        color: "#FFF"
                    }

                    MouseArea {
                        anchors.fill: parent

                        onClicked: {
                            editPopup.active = true;
                            editPopup.item.fileName = modelData
                            editPopup.item.open()
                        }
                    }

                }
            }
        }
    }

    Text {
        id: noFilesFoundMsg
        anchors.centerIn: parent

        visible: thumbnailGrid.model.length === 0

        text: "No files found, press ESC to go back"
        font.pointSize: 30
        color: "#D0D0D0"
    }

    Loader {
        id: editPopup
        active: false
        sourceComponent: EditOptions {
            onClosed: {
                main.forceActiveFocus()
                editPopup.active = false
            }
        }
    }
}
