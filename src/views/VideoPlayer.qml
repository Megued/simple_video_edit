import QtQuick 2.12
import QtMultimedia 5.12

FocusScope {
    id: videoPlayer
    property string viewName
    property string fileName

    Component.onCompleted: {
        videoPlayer.forceActiveFocus()
    }

    Keys.onEscapePressed: {
        viewHandler.setSource("VideoView.qml", {_viewName:viewName})
    }

    Video {
        anchors.fill: parent

        source: _videoHandler.getFullPath(viewName,fileName)
        autoPlay: true
        onStopped: viewHandler.setSource("VideoView.qml", {_viewName:viewName}) // just close on end
    }

}
