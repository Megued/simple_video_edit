import QtQuick 2.12
import QtQuick.Controls 2.12

FocusScope {
    Row {
        width: childrenRect.width
        height: childrenRect.height
        spacing: parent.width * 0.2
        anchors.centerIn: parent

        Button {
            height: 150
            width: 250
            text: "Edited Videos"
            font.pointSize: 15
            onClicked: viewHandler.setSource("VideoView.qml", {_viewName:"edited_videos"})

            icon.source: "qrc:/assets/icons/edited_videos.svg"
            icon.height: 50
            icon.width: 50

            background: Rectangle {
                color: "#D0D0D0"
            }
        }

        Button {
            height: 150
            width: 250
            text: "Raw Videos"
            font.pointSize: 15
            onClicked: viewHandler.setSource("VideoView.qml", {_viewName:"raw_videos"})

            icon.source: "qrc:/assets/icons/raw_videos.svg"
            icon.height: 50
            icon.width: 50

            background: Rectangle {
                color: "#D0D0D0"
            }
        }
    }
}
