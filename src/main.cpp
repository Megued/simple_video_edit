#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "libs/videohandler.h"
#include "libs/thumbnailhandler.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/Main.qml"));

    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

        if (!QDir("raw_videos").exists())
            QDir().mkdir("raw_videos");

        if (!QDir("edited_videos").exists())
            QDir().mkdir("edited_videos");

        QDir assets("assets/raw_videos");
        QStringList files = assets.entryList(QStringList() << "*.mp4", QDir::Files);

        foreach (QString fileName, files) {
            QFile::copy("assets/raw_videos/" + fileName, QDir("raw_videos").path() + QString("/%1").arg(fileName));
        }

    qmlRegisterType<ThumbnailHandler>("ThumbnailHandler", 1, 0, "ThumbnailHandler");

    engine.load(url);

    VideoHandler videoHandler;
    engine.rootContext()->setContextProperty("_videoHandler", &videoHandler);


    return app.exec();
}
