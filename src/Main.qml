import QtQuick 2.12
import QtQuick.Window 2.12
//import QtQuick.Controls 2.12

// move to EditView.QML later

Window {
    visible: true
    width: 1000
    height: 800
    title: qsTr("Video edit")
    color: "#333"
    id: window

    Loader {
        id: viewHandler

        anchors.fill: parent

        source: "views/Menu.qml"
    }
}
