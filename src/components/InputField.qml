import QtQuick 2.12
import QtQuick.Controls 2.12

Item {

    property alias text: input.text
    property bool lockInput: false
    property string coord

    width: childrenRect.width
    height: 20

    Text {
        id: coordText
        anchors.verticalCenter: input.verticalCenter

        color: "#D0D0D0"
        text: coord
        font.pointSize: 12
    }

    TextField {
        id: input
        validator: IntValidator{}

        readOnly: lockInput

        width: 60
        height: 20
        topPadding: 0
        leftPadding: 0
        bottomPadding: 1

        color: "#D0D0D0"

        anchors.left: coordText.right
        anchors.leftMargin: 5

        font.pointSize: 12

        background: Item
        {
            Rectangle
            {
                anchors.bottom: parent.bottom
                height: 1
                width: parent.width
                color: "ORANGE"
            }
        }
    }
}
