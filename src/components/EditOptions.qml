
import QtQuick 2.12
import QtQuick.Controls 2.12

import "editOptionsLogic.js" as Logic

Popup {
    id: root

    property string fileName
    property bool burnProgressDone: false

    parent: main

    focus: true
    modal: true
    dim: true

    closePolicy: Popup.CloseOnEscape

    background: Item {}

    contentItem: Item
    {
        implicitHeight: window.height
        implicitWidth: window.width

        Rectangle {
            anchors.centerIn: parent
            id: itemHolder
            height: 600
            width: 480
            border.color: "#000"
            border.width: 1

            color: "#333"

            Column {
                height: parent.height
                width: parent.width

                spacing: 30

                Item {
                    height: childrenRect.height
                    width: parent.width

                    anchors.left: parent.left
                    anchors.leftMargin: 10

                    Text {
                        id: randomNumbers
                        color: "#FFF"
                        text: "098325743287"
                        font.pointSize: 20
                        anchors.horizontalCenter: parent.horizontalCenter
                    }

                    Row {
                        anchors.top: randomNumbers.bottom
                        anchors.topMargin: 5
                        spacing: 5

                        CheckBox {
                            id: randomNumbersCheckbox
                            indicator.height: 20
                            indicator.width: 20
                        }

                        InputField {
                            id: randomNumbersX
                            coord: "X:"
                            anchors.verticalCenter: randomNumbersCheckbox.verticalCenter
                        }

                        InputField {
                            id: randomNumbersY
                            coord: "Y:"
                            anchors.verticalCenter: randomNumbersCheckbox.verticalCenter
                        }
                    }
                }

                Rectangle {
                    height: 1
                    width: 400
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#000"
                }

                Item {
                    height: childrenRect.height
                    width: parent.width

                    anchors.left: parent.left
                    anchors.leftMargin: 10

                    Rectangle {
                        id: randomShape
                        width: 100
                        height: 100
                        color: "#DC143C"
                        anchors.horizontalCenter: parent.horizontalCenter

                        gradient: Gradient {
                            GradientStop {
                                id:gradient1
                                position: 0.0
                                color: "#FF7F50"
                            }

                            GradientStop {
                                id:gradient2
                                position: 5.0
                                color: "#FFA500"
                            }
                        }
                    }

                    Row {
                        anchors.top: randomShape.bottom
                        anchors.topMargin: 10
                        spacing: 5

                        CheckBox {
                            id: randomShapeCheckbox
                            indicator.height: 20
                            indicator.width: 20
                        }

                        InputField {
                            id: randomShapeX
                            coord: "X:"
                            anchors.verticalCenter: randomShapeCheckbox.verticalCenter
                        }

                        InputField {
                            id: randomShapeY
                            coord: "Y:"
                            anchors.verticalCenter: randomShapeCheckbox.verticalCenter
                        }
                    }
                }

                Rectangle {
                    height: 1
                    width: 400
                    anchors.horizontalCenter: parent.horizontalCenter
                    color: "#000"
                }

                Item {
                    height: childrenRect.height
                    width: parent.width

                    anchors.left: parent.left
                    anchors.leftMargin: 10

                    ProgressBar {
                        id: randomSlider
                        anchors.horizontalCenter: parent.horizontalCenter

                        property bool reverse: false

                        height: 10
                        width: 300

                        from:  0
                        to: 100
                        value: 30

                        background: Rectangle {
                            width: parent.width
                            color: "#000"
                        }

                        contentItem:
                            Item {
                            width: 300
                            height: 10

                            Rectangle {
                                color: "#fba61f"
                                height: parent.height
                                width: randomSlider.visualPosition * parent.width
                            }
                        }
                    }

                    Row {
                        anchors.top: randomSlider.bottom
                        anchors.topMargin: 10
                        spacing: 5

                        CheckBox {
                            id: randomSliderCheckbox
                            indicator.height: 20
                            indicator.width: 20
                        }

                        InputField {
                            id: randomSliderX
                            coord: "X:"
                            anchors.verticalCenter: randomSliderCheckbox.verticalCenter
                        }

                        InputField {
                            id: randomSliderY
                            coord: "Y:"
                            anchors.verticalCenter: randomSliderCheckbox.verticalCenter
                        }
                    }
                }
            }

            Button {
                id: apply
                text: "Apply to video"
                visible: !burnProcess.active
                anchors.bottom: parent.bottom
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottomMargin: 20
                onClicked: {
                    // check if at least 1 option is selected
                    if (randomNumbersCheckbox.checked || randomShapeCheckbox.checked || randomSliderCheckbox.checked) {
                        Logic.initVideoBurn()
                    }
                }

                background: Rectangle {
                    color: "#D0D0D0"
                }
            }

            Loader {
                id: burnProcess
                active: false
                anchors.bottom: parent.bottom

                sourceComponent: Column {
                    width: itemHolder.width
                    height: 150
                    spacing: 10

                    Text {
                        id: processStep
                        font.pointSize: 20
                        text: "Starting process"
                        anchors.horizontalCenter: parent.horizontalCenter;
                        color: "#fba61f"
                    }

                    ProgressBar {
                        id: burnProgress
                        anchors.horizontalCenter: parent.horizontalCenter

                        height: 10
                        width: 300

                        from:  0
                        to: 100
                        value: 0

                        background: Rectangle {
                            width: parent.width
                            color: "#000"
                        }

                        contentItem:
                            Item {
                            width: 300
                            height: 10

                            Rectangle {
                                color: "#fba61f"
                                height: parent.height
                                width: burnProgress.visualPosition * parent.width
                            }
                        }
                    }

                    Button {
                        id: cancel
                        text: "Cancel"

                        visible: !closeButton.visible
                        anchors.horizontalCenter: parent.horizontalCenter
                        onClicked: {
                            Logic.cancelVideoBurn()
                        }

                        background: Rectangle {
                            color: "#D0D0D0"
                        }
                    }

                    Button {
                        id: closeButton
                        text: "Close"
                        visible: burnProgressDone
                        anchors.horizontalCenter: parent.horizontalCenter
                        onClicked: {
                            thumbnailGrid.model = _videoHandler.getFilesInFolder(_viewName)
                            close()
                        }

                        background: Rectangle {
                            color: "#D0D0D0"
                        }
                    }

                    Connections {
                        target: _videoHandler
                        onBurningProcess: {
                            burnProgress.value = percentage
                            processStep.text = step

                            if (percentage === 100) {
                                burnProgressDone = true
                            }
                        }
                    }
                }
            }
        }
    }

    onClosed: {
        if(burnProgressDone)
            thumbnailGrid.model = _videoHandler.getFilesInFolder(_viewName)
    }

    onOpened: {
        _videoHandler.setVideoData(fileName)

        numberTimer.start()
        shapeTimer.start()
        sliderTimer.start()
    }

    Timer {
        id: numberTimer
        interval: 300
        repeat: true
        triggeredOnStart: true
        onTriggered: Logic.getRandomNumber()
    }

    Timer {
        id: shapeTimer
        interval: 1000
        repeat: true
        triggeredOnStart: true
        onTriggered: Logic.getRandomGradient()
    }

    Timer {
        id: sliderTimer
        interval: 500
        repeat: true
        triggeredOnStart: true
        onTriggered: Logic.getProgressValue()
    }
}
