function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

function getRandomNumber() {
    randomNumbers.text = Math.floor(Math.random() * 99999999)

    if (burnProcess.active && randomNumbersCheckbox.checked && _videoHandler.generateClip1){
        randomNumbers.grabToImage((result)=>{
                                      _videoHandler.saveImage(result.image, 1)
                                  })
    }
}

function getRandomGradient() {

    gradient1.color = "%1".arg(getRandomColor())
    gradient1.position = Math.random()

    gradient2.color = "%1".arg(getRandomColor())
    gradient2.position = Math.random()

    if (burnProcess.active && randomShapeCheckbox.checked && _videoHandler.generateClip2){
        randomShape.grabToImage((result)=>{
                                    _videoHandler.saveImage(result.image, 2)
                                })
    }
}

function getProgressValue() {

    randomSlider.value = randomSlider.reverse ?
                randomSlider.value -= 5 :
                randomSlider.value += 5

    if (randomSlider.value >= 100)
        randomSlider.reverse = true
    else if (randomSlider.value <= 0)
        randomSlider.reverse = false

    if (burnProcess.active && randomSliderCheckbox.checked && _videoHandler.generateClip3){
        randomSlider.grabToImage((result)=>{
                                     _videoHandler.saveImage(result.image, 3)
                                 })
    }
}

function initVideoBurn(){

    randomNumbersCheckbox.checkable = false
    randomShapeCheckbox.checkable = false
    randomSliderCheckbox.checkable = false

    randomNumbersX.lockInput = true;
    randomNumbersY.lockInput = true;
    randomShapeX.lockInput = true;
    randomShapeY.lockInput = true;
    randomSliderX.lockInput = true;
    randomSliderY.lockInput = true;

    _videoHandler.initVideoBurn(randomNumbersCheckbox.checked, randomNumbersX.text, randomNumbersY.text,
                                randomShapeCheckbox.checked, randomShapeX.text, randomShapeY.text,
                                randomSliderCheckbox.checked, randomSliderX.text, randomSliderY.text)

    burnProcess.active = true
}

function cancelVideoBurn() {
    randomNumbersCheckbox.checkable = true
    randomShapeCheckbox.checkable = true
    randomSliderCheckbox.checkable = true

    randomNumbersX.lockInput = false;
    randomNumbersY.lockInput = false;
    randomShapeX.lockInput = false;
    randomShapeY.lockInput = false;
    randomSliderX.lockInput = false;
    randomSliderY.lockInput = false;

    burnProcess.active = false
    _videoHandler.cancel()

}
