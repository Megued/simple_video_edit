# README #

### What is this repository for? ###

Simple video editor made with Qt and QML that burns randomly generated overlays on a video.
Video editing is done using [libopenshot](https://github.com/OpenShot/libopenshot) functionality.

### Dependencies: ###
 Application is tested and works with Qt 5.12.5 and Qt 5.15.0 (Linux version)
 
 Included *libopenshot* is built for linux and should work in most environments that have ffmpeg installed.  
 If there are any additional dependencies to build/run the project they can be found [here](https://github.com/OpenShot/libopenshot/wiki/Linux-Build-Instructions#install-dependencies)