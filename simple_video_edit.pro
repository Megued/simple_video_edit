QT += quick multimedia gui widgets svg

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Refer to the documentation for the
# deprecated API to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

HEADERS += \
    src/libs/thumbnailhandler.h \
    src/libs/videoburner.hpp \
    src/libs/videohandler.h

SOURCES += \
        src/main.cpp \
        src/libs/thumbnailhandler.cpp \
        src/libs/videohandler.cpp

RESOURCES += \
    src/src.qrc

unix:!macx: {
INCLUDEPATH += $$PWD/3rdparty/libopenshot/include \
               $$PWD/3rdparty/libopenshot-audio/include

DEPENDPATH += $$PWD/3rdparty/libopenshot/include \
              $$PWD/3rdparty/libopenshot-audio/include

LIBS += -L$$PWD/3rdparty/libopenshot/lib/ -lopenshot
LIBS += -L$$PWD/3rdparty/libopenshot-audio/lib/ -lopenshot-audio
}

# copy assets files
copydata.commands = $(COPY_DIR) $$PWD/assets $$OUT_PWD
first.depends = $(first) copydata
export(first.depends)
export(copydata.commands)
QMAKE_EXTRA_TARGETS += first copydata

# Additional import path used to resolve QML modules in Qt Creator's code model
QML_IMPORT_PATH =

# Additional import path used to resolve QML modules just for Qt Quick Designer
QML_DESIGNER_IMPORT_PATH =

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

